#include <string>
#include <vector>
#include <stdexcept>
#include <iostream>
#include "linux.hh"
#include <cstring>
#include <cstdio>

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <glob.h>
#include <sys/stat.h>
#include <limits.h>
#include <string.h>
#include <errno.h>

std::string ac::getcwd()
{
  char buf[PATH_MAX];
  if(::getcwd(buf, PATH_MAX) == NULL)
    throw std::runtime_error{
      "Unable to get current working directory: "
	+ ac::strerror(errno)};
  return std::string{buf};
}

std::string ac::mkdtemp(const std::string& tmplate)
{
  std::vector<char> c_str_tmplate{tmplate.begin(), tmplate.end()};
  c_str_tmplate.push_back('\0');
  if(::mkdtemp(&c_str_tmplate[0]) == NULL)
    throw std::runtime_error{
      "Failed to create temporary directory from template \""
	+ tmplate + "\": " + ac::strerror(errno)};
  return std::string{c_str_tmplate.begin(), c_str_tmplate.end()-1};
}

std::vector<std::string> ac::glob(const std::string& pattern)
{
  class Glob
  {
  private:
    ::glob_t glob_buf;
    const int glob_ret;
  public:
    Glob(const std::string& pattern)
      : glob_ret{ ::glob(pattern.c_str(), 0, NULL, &glob_buf) }
    {}
    ::glob_t get() { return glob_buf; }
    int status() const { return glob_ret; };
    ~Glob() { ::globfree(&glob_buf); }
  };

  Glob gbuf {pattern};
  switch (gbuf.status())
    {
    case GLOB_NOSPACE:
      throw std::runtime_error{
	"Failed to expand glob pattern \""
	  + pattern + "\": Ran out of memory"};
      break;
    case GLOB_ABORTED:
      throw std::runtime_error{
	"Failed to expand glob pattern \""
	  + pattern + "\": Read error"};
      break;
    }

  const auto gstruct = gbuf.get();
  std::vector<std::string> results;
  for (std::size_t i = 0; i < gstruct.gl_pathc; i++)
    results.push_back(gstruct.gl_pathv[i]);
  return results;
}

bool ac::access(const std::string& path)
{
  const auto ret = ::access(path.c_str(), F_OK);
  if (ret == 0)
    return true;
  if (errno == EACCES || errno == ENOENT)
    return false;
  throw std::runtime_error{
    "Error checking for access to \""
      + path + "\":" + ac::strerror(errno)};
}

void ac::rmdir(const std::string& path)
{
  if (::rmdir(path.c_str()) != 0)
    throw std::runtime_error{
      "Error removing directory \""
	+ path + "\": " + ac::strerror(errno)};
}

void ac::mkdir(const std::string& path)
{
  if (::mkdir(path.c_str(), S_IRWXU | S_IRGRP | S_IXGRP | S_IROTH | S_IXOTH) != 0)
    throw std::runtime_error{
      "Error creating directory \""
	+ path + "\": " + ac::strerror(errno)};
}

std::string ac::realpath(const std::string& path)
{
  char buf[PATH_MAX];
  if(!::realpath(path.c_str(), buf))
    throw std::runtime_error{
      "Error getting canonical path for \""
	+ path + "\": " + ac::strerror(errno)};
  return std::string{buf};
}

std::string ac::basename(const std::string& path)
{
  return std::string{::basename(path.c_str())};
}

std::string ac::strerror(const int& err)
{
  return std::string{::strerror(err)};
}

ac::Process::Process(const std::string& command)
  : stream_open{false}
{
  open(command);
}

ac::Process::~Process()
{
  if (stream_open)
    close();
}

void ac::Process::open(const std::string& command)
{
  if (stream_open)
    throw std::runtime_error{"Cannot open process before current process closed"};
  const auto ret = ::popen(command.c_str(), "r");
  if (!ret)
    throw std::runtime_error{"Unable to open process \"" + command + "\""};
  fptr = ret;
  command_ = command;
  stream_open = true;
}

FILE* ac::Process::get()
{
  return fptr;
}

void ac::Process::close()
{
  if (!stream_open)
    throw std::runtime_error{"Tried to close already closed process"};
  const auto ret = ::pclose(fptr);
  if(ret == -1)
    throw std::runtime_error{"Error closing process \"" + command_ + "\": "
			     + ac::strerror(errno)};
  stream_open = false;
  const auto exit_status = WEXITSTATUS(ret);
  if(exit_status != 0)
    throw exit_status;
}

ac::Process ac::popen(const std::string& command)
{
  return ac::Process {command};
}
void ac::pclose(ac::Process& process)
{
  process.close();
}

bool ac::fgets(std::string& str, const std::size_t& n, FILE* stream)
{
  char buffer[n];
  const auto ret = std::fgets(buffer, n, stream);
  if (ret == NULL)
    {
      const auto fe = std::ferror(stream);
      if (fe != 0)
	throw std::runtime_error{"fgets failed on stream with error code: " + std::to_string(fe)};
      str = "";
      return false;
    }
  str = buffer;
  return true;
}
bool ac::fgets(std::string& str, const std::size_t& n, ac::Process& process)
{return ac::fgets(str, n, process.get());}

bool ac::getline(std::string& line, std::FILE* stream)
{
  char* l = NULL;
  ::size_t len = 0;
  ::ssize_t n = ::getline(&l, &len, stream);
  if (n == -1)
    {
      ::free(l);
      line = "";
      return false;
    }
  else
    {
      line = {l, static_cast<std::size_t>(n-1)};
      ::free(l);
      return true;
    }
}
bool ac::getline(std::string& line, ac::Process& process)
{return ac::getline(line, process.get());}
