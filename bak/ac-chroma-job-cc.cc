#include <iostream>
#include <fstream>
#include <stdexcept>
#include <string>
#include <functional>
#include <complex>
#include <algorithm>
#include <chrono>
#include <random>
#include <thread>
#include "util.hh"
#include "linux.hh"

const std::string inproglist_filename = "inproglist";
const std::string donelist_filename = "donelist";
const std::string deadlist_filename = "deadlist";
const std::string problist_filename = "problist";


class InProgEntry
{
private:
  const std::string cn;
public:
  InProgEntry(const std::string& conf_name) : cn{conf_name}
  {ac::append_to_file(inproglist_filename, cn);}
  ~InProgEntry()
  {
    auto lines = ac::getlines(std::ifstream{inproglist_filename});
    lines.erase(std::remove(lines.begin(), lines.end(), cn), lines.end());
    std::ofstream inproglist {inproglist_filename};
    inproglist << ac::unlines(lines);
  }
};

struct Arguments
{
  int n_confs = 1;
  int n_sources = 1;
  std::string filelist_filename = "filelist";
};

Arguments parse_arguments(int argc, char** argv)
{
  Arguments args;
  for (int i = 1; i+1 < argc; i += 2)
    {
      auto key = std::string(argv[i]);
      auto val = std::string(argv[i+1]);
      if (key == "--n-confs")
	args.n_confs = std::stoi(val);
      else if (key == "--n-sources")
	args.n_sources = std::stoi(val);
      else if (key == "--filelist")
	args.filelist_filename = val;
      else
	throw std::invalid_argument(std::string("Unrecognised argument") + key);
    }
  return args;
}

template<class RandomEngine>
std::vector<int> random_source(RandomEngine& engine, const std::vector<int>& dims)
{
  std::vector<int> source;
  for (std::size_t i = 0; i < dims.size(); i++)
    source.push_back(std::uniform_int_distribution<>{0, dims[i]-1} (engine));
  return source;
}

template<class RandomEngine>
std::complex<double> random_z_noise(RandomEngine& engine, const int& n)
{
  static constexpr double pi {3.14159265358979323846};
  double el = std::uniform_int_distribution<>{0, n-1} (engine);
  return std::exp((2 * pi * std::complex<double>{0,1})*el/((double) n));
}

std::string confname(const std::string& filename)
{
  return "qcdsf." + ac::substr_between(ac::basename(filename), "qcdsf.", ".lime");
}

std::string next_conf (const std::string& filelist_filename)
{
  auto inprog_confs = ac::getlines(std::ifstream{inproglist_filename});
  auto done_confs = ac::getlines(std::ifstream{donelist_filename});
  auto dead_confs = ac::getlines(std::ifstream{deadlist_filename});
  auto prob_confs = ac::getlines(std::ifstream{problist_filename});
  std::ifstream input {filelist_filename};
  for (std::string line; std::getline(input, line); )
    {
      auto cn = confname(line);
      if (!(ac::in(inprog_confs, cn)
	    || ac::in(prob_confs, cn)
	    || ac::in(dead_confs, cn)
	    || ac::in(done_confs, cn)
	    ))
	return ac::realpath(line);
    }
  throw std::runtime_error{"No configurations remaining"};
}

std::vector<int> parse_conf_dims(const std::string& filename)
{
  std::ifstream in {filename};
  for (std::string line; std::getline(in, line); )
    {
      if (line.find("<lx>") != std::string::npos)
	{
	  std::vector<int> dims;
	  dims.push_back(std::stoi(ac::substr_between(line, "<lx>", "</lx>")));
	  dims.push_back(std::stoi(ac::substr_between(line, "<ly>", "</ly>")));
	  dims.push_back(std::stoi(ac::substr_between(line, "<lz>", "</lz>")));
	  dims.push_back(std::stoi(ac::substr_between(line, "<lt>", "</lt>")));
	  return dims;
	}
    }
  throw std::invalid_argument("Could not parse dimensions");
}

int main(int argc, char** argv)
{
  using Clock = std::chrono::high_resolution_clock;
  const auto start_time = Clock::now();

  const auto args = parse_arguments(argc, argv);

  const auto pwd = ac::getcwd();
  std::cout << "Starting in " << pwd << std::endl;

  const auto seed_file = ac::realpath("seed");
  std::cout << "Seed file: " << seed_file << std::endl;

  const auto seed_string = ac::getline(std::ifstream{"seed"});
  std::cout << "Seed: " << seed_string << std::endl;

  const auto input_templates = ac::glob(pwd + "/*.xml.tmpl");
  std::cout << "Input templates: " << input_templates << std::endl;

  const auto output_dir = pwd + "/output";
  std::cout << "Output directory: " << output_dir << std::endl;

  if (ac::access(output_dir))
    std::cout << "Output directory exists" << std::endl;
  else
    {
      std::cout << "Creating output directory..." << std::endl;
      ac::mkdir(output_dir);
    }

  for (int i_conf = 0; i_conf < args.n_confs; i_conf++)
    {
      std::cout << "Starting configuration " << i_conf << std::endl;

      ac::Lock lock("lock");
      const auto conf_file = next_conf(args.filelist_filename);
      std::cout << "Next configuration file is " << conf_file << std::endl;
      const auto conf_name = confname(conf_file);
      std::cout << "Configuration name is " << conf_name << std::endl;
      InProgEntry {conf_name};
      lock.unlock();

      const auto dims = parse_conf_dims(conf_file);
      std::cout << "Lattice dimensions: " << dims << std::endl;

      using RandomEngine = std::default_random_engine;
      RandomEngine random_engine {};
      random_engine.seed(ac::to_seed<RandomEngine>(seed_string + conf_name));

      for (int i_source = 0; i_source < args.n_sources; i_source++)
	{
	  std::cout << "Starting source " << i_source << std::endl;

	  ac::TmpDir scratch{pwd};
	  std::cout << "Scratch directory " << scratch.path() << std::endl;

	  const auto source = random_source(random_engine, parse_conf_dims(conf_file));
	  std::cout << "Source location: " << source << std::endl;

	  std::vector<std::string> output_files;
	  std::vector<std::string> input_files;
	  for (auto input_template: input_templates)
	    {
	      const auto input_basename = ac::replace(ac::basename(input_template), ".xml.tmpl", "_" + conf_name + ".xml");
	      const auto input_filename = output_dir + "/" + input_basename;
	      const auto output_filename = output_dir + "/" + ac::replace(input_basename, "input", "output");
	      input_files.push_back(input_filename);
	      output_files.push_back(output_filename);

	      auto text = ac::getall(std::ifstream{input_template});

	      std::cout << "Filling template: " << input_filename << std::endl;
	      text = ac::replace(text, "XXTRAJXX", conf_name + "_" + ac::to_string(source,'_'));
	      text = ac::replace(text, "XXCONFXX", conf_file);
	      text = ac::replace(text, "XXRESXX", output_dir);
	      text = ac::replace(text, "XXTMPXX", scratch.path());
	      text = ac::replace(text, "XXNXXX", std::to_string(dims[0]));
	      text = ac::replace(text, "XXNYXX", std::to_string(dims[1]));
	      text = ac::replace(text, "XXNZXX", std::to_string(dims[2]));
	      text = ac::replace(text, "XXNTXX", std::to_string(dims[3]));
	      text = ac::replace(text, "XXSXXX", std::to_string(source[0]));
	      text = ac::replace(text, "XXSYXX", std::to_string(source[1]));
	      text = ac::replace(text, "XXSZXX", std::to_string(source[2]));
	      text = ac::replace(text, "XXSTXX", std::to_string(source[3]));

	      std::cout << "Filling noise..." << std::endl;
	      while (text.find("XXZ") != std::string::npos)
		{
		  const auto noise_template = "XXZ" + ac::substr_between(text, "XXZ", "XX") + "XX";
		  std::cout << "Found noise template: " << noise_template << std::endl;

		  const auto noise_real_template = ac::replace(noise_template, "IMAG", "REAL");
		  const auto noise_imag_template = ac::replace(noise_template, "REAL", "IMAG");

		  const auto noise_n = std::stoi(ac::substr_between(noise_template, "XXZ", "NOISE"));
		  std::cout << "Order: " << noise_n << std::endl;

		  const auto noise_id = ac::substr_between(noise_real_template, "NOISE", "REAL");
		  std::cout << "Id: " << noise_id << std::endl;

	      	  const auto noise = random_z_noise(random_engine, noise_n);
		  std::cout << "Filling with: " << noise << std::endl;

		  text = ac::replace(text, noise_real_template, std::to_string(noise.real()));
		  text = ac::replace(text, noise_imag_template, std::to_string(noise.imag()));
		}

	      std::ofstream output{input_filename};
	      output << text;
	    }

	  std::cout << "Input files: " << input_files << std::endl;
	  std::cout << "Output files: " << output_files << std::endl;

	  auto chroma_process = ac::popen("ac-chroma "
	  				  + ac::to_string(input_files) + " "
	  				  + ac::to_string(output_files) + " 2>&1");
	  for (std::string str; ac::fgets(str, 1024, chroma_process); )
	    std::cout << str << std::flush;
	  try {ac::pclose(chroma_process);}
	  catch (int exit_status)
	    {
	      std::cerr << "Chroma exited with code: " << exit_status << std::endl;
	      ac::append_to_file(problist_filename, conf_name);
	      return exit_status;
	    }
	  ac::append_to_file(donelist_filename, conf_name);
	}
    }
  const auto end_time = Clock::now();
  const auto run_time_m = std::chrono::duration_cast<std::chrono::minutes>(end_time-start_time).count();
  std::cout << "Ran for " << run_time_m << " minutes" << std::endl;

  return 0;

}
