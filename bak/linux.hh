#ifndef LINUX_HH
#define LINUX_HH

#include <string>
#include <vector>
#include <streambuf>
#include <cstdio>

namespace ac
{
  std::string getcwd();
  std::string mkdtemp(const std::string& tmplate);
  std::vector<std::string> glob(const std::string& pattern);
  bool access(const std::string& path);
  void rmdir(const std::string& path);
  void mkdir(const std::string& path);
  std::string realpath(const std::string& path);
  std::string basename(const std::string& path);
  std::string strerror(const int& err);

  class Process
  {
  private:
    std::string command_;
    std::FILE* fptr;
    bool stream_open;
  public:
    explicit Process(const std::string& command);
    ~Process();
    void open(const std::string& command);
    std::FILE* get();
    void close();
  };

  Process popen(const std::string& command);
  void pclose(Process& process);
  bool fgets(std::string& str, const std::size_t& n, std::FILE* stream);
  bool fgets(std::string& str, const std::size_t& n, Process& process);
  bool getline(std::string& line, std::FILE* stream);
  bool getline(std::string& line, Process& process);

}

#endif
