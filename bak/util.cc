#include <fstream>
#include <stdexcept>
#include <functional>
#include <sstream>
#include <sys/file.h>
#include <unistd.h>
#include "util.hh"
#include "linux.hh"

void ac::write_all(const std::string& filename, const std::vector<std::string>& lines)
{
  std::ofstream output {filename};
  for (auto line: lines)
    output << line << std::endl;
}

std::string ac::substr_between(const std::string& str, const std::string& mark1, const std::string& mark2)
{
  const auto mark1_pos = str.find(mark1);
  if (mark1_pos == std::string::npos)
    throw std::invalid_argument(std::string("Could not find first marker ")
				+ "\"" + mark1 + "\""
				+ " in string "
				+ "\"" + str + "\"");
  const auto mark2_pos = str.find(mark2, mark1_pos+1);
  if (mark2_pos == std::string::npos)
    throw std::invalid_argument(std::string("Could not find second marker ")
				+ "\"" + mark2 + "\""
				+ " in "
				+ "\"" + str + "\"");
  const auto start_pos = mark1_pos + mark1.length();
  return str.substr(start_pos, mark2_pos - start_pos);
}

void ac::append_to_file(const std::string& filename, const std::string& line)
{
  std::ofstream output{filename, std::ios_base::app};
  output << line << std::endl;
}

std::string ac::replace(const std::string& str,
			const std::string& substr, const std::string& repl)
{
  std::string new_str{str};
  auto substr_len = substr.length();
  auto repl_len = repl.length();
  for (std::size_t i = new_str.find(substr, 0);
       i != std::string::npos;
       i = new_str.find(substr, i))
    {
      new_str.replace(i, substr_len, repl);
      i += repl_len;
    }
  return new_str;
}


ac::TmpDir::TmpDir(const std::string& path) :
  cache_path{ac::mkdtemp(path + "/XXXXXX")}
 {}

ac::TmpDir::~TmpDir()
{
  ac::rmdir(cache_path);
}
std::string ac::TmpDir::path() const
{
  return cache_path;
}

ac::Lock::Lock(const std::string& filename) : fn{filename},
					  locked{false}
 {
   lock();
 }

ac::Lock::~Lock()
{
  if (locked)
    unlock();
}

void ac::Lock::lock()
{
  if (!locked)
    {
      fd = open(fn.c_str(), O_CREAT, S_IRUSR | S_IWUSR);
      if (fd == -1)
	throw std::runtime_error{"Unable to open lock file: " + fn};
      if (flock (fd, LOCK_EX) == -1)
	{
	  if (close (fd) == -1)
	    throw std::runtime_error{"Unable to lock or close lock file: " + fn};
	  throw std::runtime_error{"Unable to lock file: " + fn};
	}
      locked = true;
    }
  else
    throw std::runtime_error{"Tried to lock already locked lock"};
}

void ac::Lock::unlock()
{
  if (locked)
    {
      if (flock (fd, LOCK_UN) == -1)
	throw std::runtime_error{"Unable to unlock file: " + fn};
      if (close (fd) == -1)
	throw std::runtime_error{"Unable to close lock file: " + fn};
      locked = false;
    }
  else
    throw std::runtime_error{"Tried to unlock already unlocked lock"};
}

std::vector<std::string> ac::lines(const std::string& str)
{
  std::istringstream buf {str};
  std::vector<std::string> lines;
  for (std::string line; std::getline(buf, line); )
    lines.push_back(line);
  return lines;
}

std::string ac::unlines(const std::vector<std::string> strings)
{
  std::stringstream buf;
  for (const auto s: strings)
    buf << s << '\n';
  return buf.str();
}

std::string ac::getall(std::istream& input)
{
  std::stringstream buffer;
  buffer << input.rdbuf();
  return buffer.str();
}
std::string ac::getall(std::istream&& input)
{return ac::getall(input);}

std::vector<std::string> ac::getlines(std::istream& input)
{
  return ac::lines(ac::getall(input));
}
std::vector<std::string> ac::getlines(std::istream&& input)
{return ac::getlines(input);}

std::string ac::getline(std::istream& input)
{
  std::string line;
  std::getline(input, line);
  return line;
}
std::string ac::getline(std::istream&& input)
{return ac::getline(input);}
