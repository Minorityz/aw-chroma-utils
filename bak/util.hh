#ifndef UTIL_HH
#define UTIL_HH

#include <string>
#include <vector>
#include <algorithm>
#include <iostream>
#include <sstream>

namespace ac
{
  void write_all(const std::string& filename, const std::vector<std::string>& lines);
  void append_to_file(const std::string& filename, const std::string& line);

  std::string replace(const std::string& str,
		      const std::string& substr, const std::string& repl);

  std::string substr_between(const std::string& str,
			     const std::string& mark1, const std::string& mark2);

  std::vector<std::string> lines(const std::string& str);
  std::string unlines(const std::vector<std::string> strings);

  std::string getall(std::istream& input);
  std::string getall(std::istream&& input);
  std::vector<std::string> getlines(std::istream& input);
  std::vector<std::string> getlines(std::istream&& input);
  std::string getline(std::istream& input);
  std::string getline(std::istream&& input);

  template<class RandomEngine, class T>
  typename RandomEngine::result_type to_seed(const T& t)
  {
    static constexpr auto max_seed = std::numeric_limits<typename RandomEngine::result_type>::max();
    return (std::hash<T>{}(t) % max_seed);
  }

  template<class Container, class T>
  bool in(const Container& container, const T& val)
  {
    auto first = std::begin(container);
    auto last = std::end(container);
    return std::find(first, last, val) != last;
  }

  template<class T>
  std::string to_string(const std::vector<T>& vec, const char& sep = ' ')
  {
    std::stringstream stream;
    for (auto it = vec.begin() ; it != vec.end() - 1 ; it++)
      stream << *it << sep;
    stream << *(vec.end() - 1);
    return stream.str();
  }

  class TmpDir
  {
  public:
    TmpDir(const std::string& path = ".");
    ~TmpDir();
    std::string path() const;
  private:
    std::string cache_path;
  };

  class Lock
  {
  public:
    Lock(const std::string& filename);
    ~Lock();
    void lock();
    void unlock();
  private:
    std::string fn;
    int fd;
    bool locked;
  };
}

template<class T>
std::ostream& operator<<(std::ostream &os, const std::vector<T>& vec)
{
  return (os << ac::to_string(vec));
}

#endif
