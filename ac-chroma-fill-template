#!/usr/bin/env python

from __future__ import print_function
import sys
import re
import random
import cmath

(_, input_template, conf_file, source_number, bar_out_lime, bar_out_xml, mes_out_lime, mes_out_xml, temp_dir, seed) = sys.argv

with open(input_template) as infile:
    text = infile.read()

# Get configuration dimensions
conf_dims_re = re.compile(
    '\s*'
    + '\s*'.join('<lD>\s*(\d*)\s*</lD>'.replace('D', d) for d in 'xyzt')
    + '\s*')
with open(conf_file, 'rb') as f:
    for line in f:
        match = re.match(conf_dims_re, line)
        if match:
            conf_dims = tuple(
                int(d) for d in
                [match.group(i) for i in range(1, 5)])
            break

# Get random source
random.seed(seed)
source = tuple(random.randint(0, n-1) for n in conf_dims)

# Fill easy stuff
text = text.replace('XXBAROUTLIMEXX', bar_out_lime)
text = text.replace('XXBAROUTXMLXX', bar_out_xml)
text = text.replace('XXMESOUTLIMEXX', mes_out_lime)
text = text.replace('XXMESOUTXMLXX', mes_out_xml)
text = text.replace('XXCONFXX', conf_file)
text = text.replace('XXTMPXX', temp_dir)
for n, d in enumerate("XYZT"):
    text = text.replace('XXN' + d + 'XX', str(conf_dims[n]))
    text = text.replace('XXS' + d + 'XX', str(source[n]))

# Fill noise
noise_re = 'XXZ(\d*)NOISE(.*?)REALXX'
while True:
    match = re.search(noise_re, text)
    if not match:
        break
    noise_tag = match.group(0)
    noise_degree = int(match.group(1))
    noise_id = match.group(2)
    random.seed(seed + noise_id)
    noise = cmath.exp(2*cmath.pi*random.randint(0, noise_degree-1)*1j/noise_degree)
    text = text.replace(noise_tag, str(noise.real))
    text = text.replace(noise_tag.replace('REAL', 'IMAG'), str(noise.imag))

print(text)
