import xml.etree.ElementTree as ET
import xml.dom.minidom as minidom
import uuid


def random_id():
    return str(uuid.uuid4())[:8]


class Node(object):

    def __init__(self, name, text=None):
        self.name = name
        if text is not None:
            text = str(text)
        self._text = text
        self._children = []

    @property
    def text(self):
        return self._text

    @text.setter
    def text(self, value):
        assert len(self._children) == 0, (
            "Can't add text to an XML node with children!")
        self._text = value

    def append(self, node):
        assert self.text is None, (
            "Can't add children to an XML node with text!")
        self._children.append(node)

    # def __getitem__(self, key):
    #     if isinstance(key, int):
    #         return self._children(key)
    #     else:
    #         return [c for c in self._children if c.name == key]

    # def __iter__(self):
    #     return iter(self._children)

    def _to_Element(self):
        me = ET.Element(self.name)
        if self.text is not None:
            me.text = self.text
        else:
            for child in self._children:
                me.append(child._to_Element())
        return me

    def __str__(self):
        elem = self._to_Element()
        rough_string = ET.tostring(elem, 'utf-8')
        reparsed = minidom.parseString(rough_string)
        return reparsed.toprettyxml(indent="  ")


class ChromaInputObject(object):
    def __init__(self):
        pass

    def __str__(self):
        return str(self.to_node())


class ChromaInput(ChromaInputObject):
    def __init__(self, inline_measurements):
        self.inline_measurements = inline_measurements

    def to_node(self):
        root = Node("chroma")

        param = Node("Param")
        root.append(param)
        param.append(Node("nrow", "XXNXXX XXNYXX XXNZXX XXNTXX"))

        inline_meas = Node("InlineMeasurements")
        param.append(inline_meas)
        for im in self.inline_measurements:
            inline_meas.append(im.to_node())

        rng = Node("RNG")
        root.append(rng)

        seed = Node("Seed")
        rng.append(seed)
        seed.append(Node("elem", 11))
        seed.append(Node("elem", 11))
        seed.append(Node("elem", 11))
        seed.append(Node("elem", 0))

        cfg = Node("Cfg")
        root.append(cfg)
        cfg.append(Node("cfg_type", "ILDG"))
        cfg.append(Node("cfg_file", "XXCONFXX"))

        return root


class Source(ChromaInputObject):
    def __init__(self, name=None):
        if name is None:
            self.name = "source" + random_id()
        else:
            self.name = name

    def to_node(self):
        root = Node("elem")
        root.append(Node("Name", "MAKE_SOURCE"))
        root.append(Node("Frequency", 1))

        param = Node("Param")
        root.append(param)
        param.append(Node("version", 6))

        source = Node("Source")
        param.append(source)
        source.append(Node("version", 3))
        source.append(Node("SourceType", "SHELL_SOURCE"))
        source.append(Node("j_decay", 3))
        source.append(Node("t_srce", "XXSXXX XXSYXX XXSZXX XXSTXX"))
        source.append(Node("quark_smear_lastP", "false"))

        smearing_param = Node("SmearingParam")
        source.append(smearing_param)
        smearing_param.append(Node("wvf_kind", "GAUGE_INV_JACOBI"))
        smearing_param.append(Node("wvf_param", 0.21))
        smearing_param.append(Node("wvfIntPar", 75))
        smearing_param.append(Node("no_smear_dir", 3))

        displacement = Node("Displacement")
        source.append(displacement)
        displacement.append(Node("version", 1))
        displacement.append(Node("DisplacementType", "NONE"))

        named_object = Node("NamedObject")
        root.append(named_object)
        named_object.append(Node("gauge_id", "default_gauge_field"))
        named_object.append(Node("source_id", self.name))

        return root


class BiCGStabInverter:
    def __init__(self):
        pass

    def to_node(self, propagator):
        root = Node("InvertParam")
        root.append(Node("invType", "BICGSTAB_INVERTER"))
        root.append(Node("RsdBiCGStab", 5.0e-11))
        root.append(Node("MaxBiCGStab", 10000))
        return root


class ReliableBiCGStabMPSLRCQCDSFInverter:
    def __init__(self):
        pass

    def to_node(self, propagator):
        root = Node("InvertParam")
        root.append(
            Node("invType", "RELIABLE_BICGSTAB_MP_SLRC_INVERTER-QCDSF"))
        root.append(Node("MaxIter", 10000))
        root.append(Node("RsdTarget", 1e-12))
        root.append(Node("Delta", 0.01))

        cloverparams = Node("CloverParams")
        root.append(cloverparams)
        cloverparams.append(Node("Kappa", propagator.kappa))
        cloverparams.append(Node("clovCoeff", 2.65))

        fermstate = Node("FermState")
        cloverparams.append(fermstate)
        fermstate.append(Node("Name", "SLIC_FERM_STATE"))
        fermstate.append(Node("version", "2"))
        fermstate.append(Node("n_smear", "1"))
        fermstate.append(Node("rho", "0.1"))
        fermstate.append(Node("smear_in_this_dirP", "1 1 1 1"))

        fermbc = Node("FermionBC")
        fermstate.append(fermbc)
        fermbc.append(Node("FermBC", "SIMPLE_FERMBC"))
        fermbc.append(Node("boundary", "1 1 1 -1"))

        return root


class ReliableBiCGStabMPSLRCFeynHellQCDSFInverter:
    def __init__(self):
        pass

    def to_node(self, propagator):
        root = Node("InvertParam")
        root.append(
            Node("invType", "RELIABLE_BICGSTAB_MP_SLRC_FEYNHELL_INVERTER-QCDSF"))
        root.append(Node("MaxIter", 10000))
        root.append(Node("RsdTarget", 1e-12))
        root.append(Node("Delta", 0.01))

        cloverparams = Node("CloverParams")
        root.append(cloverparams)
        cloverparams.append(Node("Kappa", propagator.kappa))
        cloverparams.append(Node("clovCoeff", 2.65))

        fhparams = Node("FeynHellParam")
        root.append(fhparams)
        for op in propagator.operators:
            fhparams.append(op.to_node())

        fermstate = Node("FermState")
        cloverparams.append(fermstate)
        fermstate.append(Node("Name", "SLIC_FERM_STATE"))
        fermstate.append(Node("version", "2"))
        fermstate.append(Node("n_smear", "1"))
        fermstate.append(Node("rho", "0.1"))
        fermstate.append(Node("smear_in_this_dirP", "1 1 1 1"))

        fermbc = Node("FermionBC")
        fermstate.append(fermbc)
        fermbc.append(Node("FermBC", "SIMPLE_FERMBC"))
        fermbc.append(Node("boundary", "1 1 1 -1"))

        return root


class Propagator(ChromaInputObject):
    def __init__(self, source, kappa,
                 inverter=ReliableBiCGStabMPSLRCQCDSFInverter(), name=None):
        if name is None:
            self.name = "-".join(["prop", str(kappa), random_id()])
        else:
            self.name = name
        self.source = source
        self.kappa = kappa
        self.inverter = inverter

    def to_node(self):
        root = Node("elem")
        root.append(Node("Name", "PROPAGATOR"))
        root.append(Node("Frequency", 1))

        params = Node("Param")
        root.append(params)
        params.append(Node("version", 10))
        params.append(Node("quarkSpinType", "FULL"))
        params.append(Node("obsvP", "false"))
        params.append(Node("numRetries", 1))

        action = Node("FermionAction")
        params.append(action)
        action.append(Node("FermAct", "SLRC"))
        action.append(Node("Kappa", self.kappa))
        action.append(Node("clovCoeff", 2.65))

        fermstate = Node("FermState")
        action.append(fermstate)
        fermstate.append(Node("Name", "SLIC_FERM_STATE"))
        fermstate.append(Node("n_smear", 1))
        fermstate.append(Node("rho", 0.1))
        fermstate.append(Node("orthog_dir", 5))

        fermbc = Node("FermionBC")
        fermstate.append(fermbc)
        fermbc.append(Node("FermBC", "SIMPLE_FERMBC"))
        fermbc.append(Node("boundary", "1 1 1 -1"))

        params.append(self.inverter.to_node(self))

        namedobj = Node("NamedObject")
        root.append(namedobj)
        namedobj.append(Node("gauge_id", "default_gauge_field"))
        namedobj.append(Node("source_id", self.source.name))
        namedobj.append(Node("prop_id", self.name))

        return root


class FHPropagator(Propagator):
    def __init__(self, source, kappa, operators,
                 inverter=ReliableBiCGStabMPSLRCFeynHellQCDSFInverter(),
                 name=None):
        if name is None:
            name = "-".join(
                ["fh_prop", str(kappa)]
                + ["".join([
                    "lam", str(o.lam),
                    "op", str(o.operator),
                    "mom", "_".join([str(i) for i in o.momentum])])
                   for o in operators]
                + [random_id()])
        super(FHPropagator, self).__init__(source, kappa, name=name)
        self.operators = operators
        self.inverter = inverter

    def to_node(self):
        root = Node("elem")
        root.append(Node("Name", "PROPAGATOR"))
        root.append(Node("Frequency", 1))

        params = Node("Param")
        root.append(params)
        params.append(Node("version", 10))
        params.append(Node("quarkSpinType", "FULL"))
        params.append(Node("obsvP", "false"))
        params.append(Node("numRetries", 1))

        action = Node("FermionAction")
        params.append(action)
        action.append(Node("FermAct", "UNPREC_SLRC_FEYNHELL"))
        action.append(Node("Kappa", self.kappa))
        action.append(Node("clovCoeff", 2.65))

        fhparams = Node("FeynHellParam")
        action.append(fhparams)
        for op in self.operators:
            fhparams.append(op.to_node())

        fermstate = Node("FermState")
        action.append(fermstate)
        fermstate.append(Node("Name", "SLIC_FERM_STATE"))
        fermstate.append(Node("n_smear", 1))
        fermstate.append(Node("rho", 0.1))
        fermstate.append(Node("orthog_dir", 5))

        fermbc = Node("FermionBC")
        fermstate.append(fermbc)
        fermbc.append(Node("FermBC", "SIMPLE_FERMBC"))
        fermbc.append(Node("boundary", "1 1 1 -1"))

        params.append(self.inverter.to_node(self))

        namedobj = Node("NamedObject")
        root.append(namedobj)
        namedobj.append(Node("gauge_id", "default_gauge_field"))
        namedobj.append(Node("source_id", self.source.name))
        namedobj.append(Node("prop_id", self.name))

        return root


class FHOperator(ChromaInputObject):
    def __init__(self, lam, operator, momentum=(0, 0, 0), z_noise=1):
        self.lam = lam
        self.operator = operator
        self.momentum = momentum
        self.noise_id = random_id()
        self.z_noise = z_noise

    def to_node(self):
        root = Node("elem")
        root.append(Node("LambdaReal", str(self.lam.real)))
        root.append(Node("LambdaImag", str(self.lam.imag)))
        root.append(Node("Operator", str(self.operator)))
        root.append(Node("Momentum", " ".join(
            [str(i) for i in self.momentum])))
        root.append(Node("Source", "XXSXXX XXSYXX XXSZXX XXSTXX"))
        root.append(Node("NoiseReal", "XXZ" + str(self.z_noise) + "NOISE" + self.noise_id + "REALXX"))
        root.append(Node("NoiseImag", "XXZ" + str(self.z_noise) + "NOISE" + self.noise_id + "IMAGXX"))
        return root


class Sink(ChromaInputObject):
    def __init__(self, propagator, name=None):
        if name is None:
            self.name = "-".join(["smeared", propagator.name, random_id()])
        else:
            self.name = name
        self.propagator = propagator

    def to_node(self):
        root = Node("elem")
        root.append(Node("Name", "SINK_SMEAR"))
        root.append(Node("Frequency", 1))

        param = Node("Param")
        root.append(param)
        param.append(Node("version", 5))

        sink = Node("Sink")
        param.append(sink)
        sink.append(Node("version", 2))
        sink.append(Node("SinkType", "SHELL_SINK"))
        sink.append(Node("j_decay", 3))

        smearing_param = Node("SmearingParam")
        sink.append(smearing_param)
        smearing_param.append(Node("wvf_kind", "GAUGE_INV_JACOBI"))
        smearing_param.append(Node("wvf_param", 0.21))
        smearing_param.append(Node("wvfIntPar", 75))
        smearing_param.append(Node("no_smear_dir", 3))

        displacement = Node("Displacement")
        sink.append(displacement)
        displacement.append(Node("version", 1))
        displacement.append(Node("DisplacementType", "NONE"))

        named_object = Node("NamedObject")
        root.append(named_object)
        named_object.append(Node("gauge_id", "default_gauge_field"))
        named_object.append(Node("prop_id", self.propagator.name))
        named_object.append(Node("smeared_prop_id", self.name))

        return root


class BaryonSpectrum(ChromaInputObject):
    def __init__(self, sink_pairs, mom2_max=0):
        self.sink_pairs = sink_pairs
        self.mom2_max = mom2_max

    def to_node(self):
        root = Node("elem")
        root.append(Node("Name", "BARYON_SPECTRUM-QCDSF"))
        root.append(Node("Frequency", 1))
        root.append(Node("lime_file", "XXBAROUTLIMEXX"))
        root.append(Node("xml_file", "XXBAROUTXMLXX"))

        param = Node("Param")
        root.append(param)
        param.append(Node("version", 1))
        param.append(Node("fwdbwd_average", "false"))
        param.append(Node("time_rev", "true"))
        param.append(Node("mom2_max", str(self.mom2_max)))
        param.append(Node("avg_equiv_mom", "false"))
        param.append(Node("xml", "false"))
        param.append(Node("lime", "true"))

        named_object = Node("NamedObject")
        root.append(named_object)
        named_object.append(Node("gauge_id", "default_gauge_field"))

        sink_pairs = Node("sink_pairs")
        named_object.append(sink_pairs)
        for sink_pair in self.sink_pairs:
            sp = Node("elem")
            sink_pairs.append(sp)
            sp.append(Node("first_id", sink_pair[0].name))
            sp.append(Node("second_id", sink_pair[1].name))

        return root


class MesonSpectrum(ChromaInputObject):
    def __init__(self, sink_pairs, mom2_max=0):
        self.sink_pairs = sink_pairs
        self.mom2_max = mom2_max

    def to_node(self):
        root = Node("elem")
        root.append(Node("Name", "MESON_SPECTRUM-QCDSF"))
        root.append(Node("Frequency", 1))
        root.append(Node("lime_file", "XXMESOUTLIMEXX"))
        root.append(Node("xml_file", "XXMESOUTXMLXX"))

        param = Node("Param")
        root.append(param)
        param.append(Node("version", 1))
        param.append(Node("fwdbwd_average", "false"))
        param.append(Node("time_rev", "false"))
        param.append(Node("mom2_max", str(self.mom2_max)))
        param.append(Node("avg_equiv_mom", "false"))
        param.append(Node("xml", "false"))
        param.append(Node("lime", "true"))

        named_object = Node("NamedObject")
        root.append(named_object)
        named_object.append(Node("gauge_id", "default_gauge_field"))

        sink_pairs = Node("sink_pairs")
        named_object.append(sink_pairs)
        for sink_pair in self.sink_pairs:
            sp = Node("elem")
            sink_pairs.append(sp)
            sp.append(Node("first_id", sink_pair[0].name))
            sp.append(Node("second_id", sink_pair[1].name))

        return root


class Write(ChromaInputObject):
    def __init__(self, obj):
        self.obj = obj

    def to_node(self):
        root = Node("elem")
        root.append(Node("Name", "QIO_WRITE_NAMED_OBJECT"))
        root.append(Node("Frequency", 1))

        named_object = Node("NamedObject")
        root.append(named_object)
        named_object.append(Node("object_id", self.obj.name))
        if any(isinstance(self.obj, x) for x in [Propagator, Sink]):
            named_object.append(Node("object_type", "LatticePropagator"))
        else:
            raise ValueError(
                "Don't know how to write object of type" + str(type(self.obj)))

        fil = Node("File")
        root.append(fil)
        fil.append(Node("file_name", "XXTMPXX/" + self.obj.name + ".lime"))
        fil.append(Node("file_volfmt", "SINGLEFILE"))

        return root


class Read(ChromaInputObject):
    def __init__(self, obj):
        self.obj = obj

    def to_node(self):
        root = Node("elem")
        root.append(Node("Name", "QIO_READ_NAMED_OBJECT"))
        root.append(Node("Frequency", 1))

        named_object = Node("NamedObject")
        root.append(named_object)
        named_object.append(Node("object_id", self.obj.name))
        if any(isinstance(self.obj, x) for x in [Propagator, Sink]):
            named_object.append(Node("object_type", "LatticePropagator"))
        else:
            raise ValueError(
                "Don't know how to write object of type" + str(type(self.obj)))

        fil = Node("File")
        root.append(fil)
        fil.append(Node("file_name", "XXTMPXX/" + self.obj.name + ".lime"))

        return root


class Erase(ChromaInputObject):
    def __init__(self, obj):
        self.obj = obj

    def to_node(self):
        root = Node("elem")
        root.append(Node("Name", "ERASE_NAMED_OBJECT"))
        root.append(Node("Frequency", 1))

        named_object = Node("NamedObject")
        root.append(named_object)
        named_object.append(Node("object_id", self.obj.name))

        return root


def create_input(baryon_spectrum=None, meson_spectrum=None):
    baryon_spectrum = [] if baryon_spectrum is None else baryon_spectrum
    meson_spectrum = [] if meson_spectrum is None else meson_spectrum
    sinks = []
    for sink_pair in baryon_spectrum.sink_pairs + meson_spectrum.sink_pairs:
        sinks.append(sink_pair[0])
        sinks.append(sink_pair[1])
    sinks = set(sinks)
    propagators = set(sink.propagator for sink in sinks)
    sources = set(prop.source for prop in propagators)

    ims = []
    for source in sources:
        ims.append(source)
        for prop in propagators:
            if prop.source == source:
                ims.append(prop)
                for sink in sinks:
                    if sink.propagator == prop:
                        ims.append(sink)
    ims.append(baryon_spectrum)
    ims.append(meson_spectrum)

    input1 = ChromaInput(ims)
    return input1


def create_split_input(baryon_spectrum, meson_spectrum):
    sinks = []
    for sink_pair in baryon_spectrum.sink_pairs + meson_spectrum.sink_pairs:
        sinks.append(sink_pair[0])
        sinks.append(sink_pair[1])
    sinks = set(sinks)
    propagators = set(sink.propagator for sink in sinks)
    sources = set(prop.source for prop in propagators)

    ims1 = []
    ims2 = []
    for source in sources:
        ims1.append(source)
        for prop in propagators:
            if prop.source == source:
                ims1.append(prop)
                for sink in sinks:
                    if sink.propagator == prop:
                        ims1.append(sink)
                        ims1.append(Write(sink))
                        ims1.append(Erase(sink))
                        ims2.append(Read(sink))
                ims1.append(Erase(prop))
        ims1.append(Erase(source))
    ims2.append(baryon_spectrum)
    ims2.append(meson_spectrum)

    input1 = ChromaInput(ims1)
    input2 = ChromaInput(ims2)
    return input1, input2

# def create_input(props, fh_props, split=True):
#     source = Source()

#     props = [Propagator(source, k) for k in props]
#     fh_props = [FHPropagator(
#         source,
#         p[0],
#         [FHOperator(o[0], o[1], o[2]) for o in p[1]])
#                 for p in fh_props]

#     all_props = (props + fh_props)
#     all_sinks = [Sink(p) for p in all_props]

#     sink_pairs = [(p1, p2) for p1 in all_sinks for p2 in all_sinks]

#     baryon_spectrum = BaryonSpectrum(sink_pairs)
#     meson_spectrum = MesonSpectrum(sink_pairs)

#     if split:
#         ims = [source]
#         for p, s in zip(all_props, all_sinks):
#             ims.append(p)
#             ims.append(s)
#             ims.append(Write(s))
#             ims.append(Erase(p))
#             ims.append(Erase(s))
#         input1 = ChromaInput(ims)

#         input2 = ChromaInput(
#             [Read(s) for s in all_sinks]
#             + [baryon_spectrum, meson_spectrum])
#         return input1, input2
#     else:
#         input1 = ChromaInput(
#             [source]
#             + [p for p in all_props]
#             + [s for s in all_sinks]
#             + [baryon_spectrum, meson_spectrum])
#         return input1
