#include "util.hpp"

#include <iostream>
#include <sstream>

#include <boost/filesystem.hpp>
#include <boost/regex.hpp>

namespace ac {

  std::vector<std::string> arguments(int argc, char** argv) {
    std::vector<std::string> args;
    for (int i = 1; i != argc; i++)
      args.push_back(argv[i]);
    return args;
  }

  std::string getall(std::istream& is) {
    std::stringstream buffer;
    buffer << is.rdbuf();
    return buffer.str();
  }
  std::string getall(std::istream&& is) {return getall(is);}

  std::vector<std::string> getlines(std::istream& is) {
    std::vector<std::string> lines;
    for (std::string line; std::getline(is, line); )
      lines.push_back(line);
    return lines;
  }
  std::vector<std::string> getlines(std::istream&& is) {return getlines(is);}

  std::string firstline(std::istream& is) {
    std::string line;
    std::getline(is, line);
    return line;
  }
  std::string firstline(std::istream&& is) {return firstline(is);}

  std::vector<std::string> split(const std::string& s,
				 const char& delimeter) {
    std::stringstream buffer{s};
    std::vector<std::string> words;
    for (std::string word; std::getline(buffer, word, delimeter);)
      words.push_back(word);
    return words;
  }

  bool exists(const std::string& filename) {
    return boost::filesystem::exists(boost::filesystem::path(filename));
  }

  std::string pwd() {
    return boost::filesystem::current_path().string();
  }

  void rename(const std::string& source, const std::string& destination) {
    boost::filesystem::rename(source, destination);
  }

  void append(const std::string& filename, const std::string& line) {
    std::ofstream outfile(filename, std::ios_base::app);
    outfile << line << std::endl;
  }

  std::string& replace(std::string& string,
		       const std::string& target,
		       const std::string& replacement) {
    auto target_len = target.size();
    auto replacement_len = replacement.size();
    std::size_t pos(0);
    while ((pos = string.find(target, pos)) != std::string::npos) {
      string.replace(pos, target_len, replacement);
      pos += replacement_len;
    }
    return string;
  }

  std::string& replace(std::string& string,
		      const std::map<std::string, std::string>& translation) {
    for (const auto& trans : translation)
      replace(string, trans.first, trans.second);
    return string;
  }

  bool match(const std::string& string,
	     const std::string& regex,
	     std::vector<std::string>& matches) {
    boost::cmatch match_results;
    const auto result = boost::regex_match(string.c_str(),
					   match_results,
					   boost::regex{regex});
    if (result)
      for (const auto& m : match_results)
	matches.push_back(m);
    return result;
  }

  std::vector<std::vector<std::string>> regex_findall(const std::string& string,
						      const std::string& regex) {
    const boost::regex bregex{regex};
    boost::match_results<std::string::const_iterator> what;
    std::vector<std::vector<std::string>> results;
    auto begin = string.cbegin();
    auto end = string.cend();
    while (boost::regex_search(begin, end, what, bregex)) {
      std::vector<std::string> subresults;
      for (const auto& m : what)
	subresults.push_back(std::string(m.first, m.second));
      results.push_back(subresults);
      begin = what[0].first + 1;
    }
    return results;
  }

  void mkdir(const std::string& path) {
    boost::filesystem::create_directory(boost::filesystem::path(path));
  }

  std::string canonicalpath(const std::string& path) {
    return boost::filesystem::canonical(boost::filesystem::path(path)).string();
  }

  std::string absolutepath(const std::string& path) {
    return boost::filesystem::absolute(boost::filesystem::path(path)).string();
  }

  std::string basename(const std::string& filename) {
    return boost::filesystem::path(filename).filename().string();
  }
  std::string filename(const std::string& path) {return basename(path);}

  bool contains(const std::string& string, const std::string& substring) {
    return string.find(substring) != std::string::npos;
  }
  bool contains(const char* string, const char* substring)
  {return contains(std::string{string}, std::string{substring});}
  bool contains(const std::string& string, const char* substring)
  {return contains(string, std::string{substring});}
  bool contains(const char* string, const std::string& substring)
  {return contains(std::string{string}, substring);}

  std::string stem(const std::string& path) {
    return boost::filesystem::path(path).stem().string();
  }

  std::string fullstem(const std::string& path) {
    auto bpath = boost::filesystem::path(path);
    auto spath = bpath.stem();
    while (spath != bpath) {
      bpath = spath;
      spath = spath.stem();
    }
    return spath.string();
  }

  int run_command(const std::string& command, std::ostream& output) {
    auto stream = popen(("{ " + command + " ; } 2>&1").c_str(), "r");
    if (stream == NULL)
      throw std::runtime_error{
	std::string{"Error starting process: "} + ::strerror(errno)};

    static const int buffer_size {80};
    char buffer[buffer_size];
    while(fgets(buffer, sizeof(buffer_size), stream) != NULL)
      output << buffer << std::flush;

    auto exit_status = pclose(stream);
    if (exit_status == -1)
      throw std::runtime_error{
	std::string{"Error ending process: "} + ::strerror(errno)};
    return exit_status;

  }
}
