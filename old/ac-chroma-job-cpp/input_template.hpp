#ifndef INPUT_TEMPLATE_HPP
#define INPUT_TEMPLATE_HPP

#include <string>
#include <vector>
#include "noise_template.hpp"

class Configuration;

class InputTemplate {
public:
  explicit InputTemplate(const std::string& path);

  std::string write(std::string input_dir,
		    std::string output_dir,
		    std::string temp_dir,
		    const Configuration&,
		    std::string seed) const;

  std::string name() const;

private:
  std::string path_;
  std::string template_;
  std::vector<NoiseTemplate> noise_templates_;
};

#endif
