#ifndef NOISE_TEMPLATE_HPP
#define NOISE_TEMPLATE_HPP

#include <string>
#include <vector>

class NoiseTemplate {
public:
  NoiseTemplate() = default;
  NoiseTemplate(const std::string&);

  std::string raw() const;
  std::string fill(const std::string& seed) const;

private:
  std::string template_;
  int degree_;
  std::string id_;
};

std::vector<NoiseTemplate> find_noise_templates(const std::string&);

#endif
