#include <iostream>
#include "temp_dir.hpp"

namespace ac {
  TempDir::TempDir()
    : path_{boost::filesystem::unique_path("%%%%-%%%%-%%%%-%%%%.tmp")}
    {boost::filesystem::create_directory(path_);}

  TempDir::~TempDir() {
    boost::filesystem::remove_all(path_);
  }

  std::string TempDir::path() const {return path_.string();}
}
