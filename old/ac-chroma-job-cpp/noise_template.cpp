#include "util.hpp"
#include "noise_template.hpp"

#include <vector>
#include <complex>
#include <random>
#include <string>

NoiseTemplate::NoiseTemplate(const std::string& slot)
  : NoiseTemplate()
{
  std::vector<std::string> matches;
  const static std::string regex = "XXZ(\\d)NOISE([^\\s]*?)XX";

  if (!ac::match(slot, regex, matches))
    throw std::runtime_error("Invalid noise slot found");

  template_ = matches[0];
  degree_ = std::stoi(matches[1]);
  id_ = matches[2];
}

std::string NoiseTemplate::raw() const {
  return template_;
}

std::complex<double> random_zn(const int& n, const std::string& seed) {
  auto seed_seq = ac::seed_seq(seed);
  std::default_random_engine generator(seed_seq);
  std::uniform_int_distribution<int> distribution(0, n-1);
  int k {distribution(generator)};
  return std::exp((std::complex<double>{0, 1} * (2 * ac::pi * k)) / (n * 1.0));
}

std::string NoiseTemplate::fill(const std::string& seed) const {
  const auto z_noise = random_zn(degree_, seed + id_);
  if (ac::contains(template_, "REAL"))
    return std::to_string(z_noise.real());
  else
    return std::to_string(z_noise.imag());
}

std::vector<NoiseTemplate> find_noise_templates(const std::string& s) {
  std::vector<NoiseTemplate> templates;
  for (const auto& noise_slot_match :
	 ac::regex_findall(s, "XXZ\\dNOISE[^\\s]*?XX"))
    templates.push_back(NoiseTemplate{noise_slot_match[0]});
  return templates;
}
