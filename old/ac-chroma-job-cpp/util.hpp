#ifndef UTIL_HPP
#define UTIL_HPP

#include <vector>
#include <string>
#include <iostream>
#include <algorithm>
#include <map>

#include "temp_dir.hpp"

namespace ac {

  const double pi {3.14159265359};

  std::vector<std::string> arguments(int argc, char** argv);

  std::string getall(std::istream&);
  std::string getall(std::istream&&);

  std::string firstline(std::istream&);
  std::string firstline(std::istream&&);

  std::vector<std::string> getlines(std::istream&);
  std::vector<std::string> getlines(std::istream&&);

  std::vector<std::string> split(const std::string&, const char& delimeter);

  void mkdir(const std::string&);
  std::string pwd();
  std::string canonicalpath(const std::string&);
  std::string absolutepath(const std::string&);
  std::string stem(const std::string&);
  std::string fullstem(const std::string&);
  std::string filename(const std::string& path);
  bool exists(const std::string& filename);
  std::string basename(const std::string& filename);
  void append(const std::string& filename, const std::string&);
  void rename(const std::string& source, const std::string& destination);

  std::string& replace(std::string& string,
		       const std::string& target,
		       const std::string& replacement);
  std::string& replace(std::string& string,
		       const std::map<std::string, std::string>& translation);

  bool match(const std::string& string, const std::string& regex,
	     std::vector<std::string>& matches);
  std::vector<std::vector<std::string>> regex_findall(const std::string& string,
		     const std::string& regex);

  int run_command(const std::string&, std::ostream& = std::cout);

  template <typename Container, typename Element>
  std::size_t count(const Container& container, const Element& element) {
    using std::begin;
    using std::end;
    return std::count(begin(container), end(container), element);
  }

  template <typename Container, typename Element>
  bool contains(const Container& container, const Element& element) {
    using std::begin;
    using std::end;
    return (std::find(begin(container), end(container), element) != end(container));
  }
  template <>
  bool contains(const std::string& , const std::string&);
  bool contains(const char* , const char*);
  bool contains(const std::string& , const char*);
  bool contains(const char* , const std::string&);

  template <typename Sequence>
  std::seed_seq seed_seq(const Sequence& seq) {
    using std::begin;
    using std::end;
    return std::seed_seq(begin(seq), end(seq));
  }

  template <typename Element>
  std::vector<Element> slice(const std::vector<Element>& seq,
		    const std::size_t& start, const std::size_t& finish) {
    return std::vector<Element>(seq.begin()+start, seq.begin()+finish);
  }
}

template <typename Element>
std::ostream& operator<<(std::ostream& os, const std::vector<Element>& vec) {
  auto iter = vec.begin();
  auto last = vec.end() - 1;
  while (iter != last) {
    os << *iter << std::endl;
    iter++;
  }
  os << *last;
  return os;
}

template <typename T>
std::istream& operator>>(std::istream& is, std::vector<T>& ts) {
  T t;
  while (is >> t)
    ts.push_back(t);
  return is;
}

#endif
