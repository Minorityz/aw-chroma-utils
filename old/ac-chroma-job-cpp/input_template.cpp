#include <random>
#include <vector>
#include <map>

#include "input_template.hpp"
#include "configuration.hpp"
#include "util.hpp"

InputTemplate::InputTemplate(const std::string& path)
  : path_{ac::canonicalpath(path)},
    template_{ac::getall(std::ifstream{path})},
    noise_templates_{find_noise_templates(template_)}
    {}

std::vector<int> random_source(const std::vector<int>& dims,
			       const std::string& seed) {
  auto seed_seq = ac::seed_seq(seed);
  std::vector<int> source;
  std::default_random_engine generator(seed_seq);
  for (const auto& dim: dims) {
    std::uniform_int_distribution<int> distribution(0, dim);
    source.push_back(distribution(generator));
  }
  return source;
}

std::string InputTemplate::write(std::string input_dir,
				 std::string output_dir,
				 std::string temp_dir,
				 const Configuration& conf,
				 std::string seed) const {

  // Make sure paths are canonical
  input_dir = ac::canonicalpath(input_dir);
  output_dir = ac::canonicalpath(output_dir);
  temp_dir = ac::canonicalpath(temp_dir);

  // Add configuration details to seed
  seed += ac::filename(conf.path()) + std::to_string(conf.source());

  // Create replacement map
  const auto dims = conf.dims();
  const auto source = random_source(dims, seed);
  std::map<std::string, std::string> replacements;
  replacements["XXTRAJXX"] = conf.name()
    + "_" + std::to_string(source[0])
    + "_" + std::to_string(source[1])
    + "_" + std::to_string(source[2])
    + "_" + std::to_string(source[3]);
  replacements["XXCONFXX"] = conf.path();
  replacements["XXRESXX"] = output_dir;
  replacements["XXTMPXX"] = temp_dir;
  replacements["XXNXXX"] = std::to_string(dims[0]);
  replacements["XXNYXX"] = std::to_string(dims[1]);
  replacements["XXNZXX"] = std::to_string(dims[2]);
  replacements["XXNTXX"] = std::to_string(dims[3]);
  replacements["XXSXXX"] = std::to_string(source[0]);
  replacements["XXSYXX"] = std::to_string(source[1]);
  replacements["XXSZXX"] = std::to_string(source[2]);
  replacements["XXSTXX"] = std::to_string(source[3]);
  for (const auto& noise_template : noise_templates_)
    replacements[noise_template.raw()] = noise_template.fill(seed);

  // Fill template
  auto filled_template = template_;
  ac::replace(filled_template, replacements);

  // Write file
  const auto outname = input_dir + "/" + name() + "_" + replacements["XXTRAJXX"]
    + ".xml";
  std::ofstream outfile{outname};
  outfile << filled_template;

  return outname;
}

std::string InputTemplate::name() const {
  return ac::fullstem(path_);
}
