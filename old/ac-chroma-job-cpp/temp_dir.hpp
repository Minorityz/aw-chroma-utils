#ifndef TEMP_DIR_HPP
#define TEMP_DIR_HPP

#include <string>

#include <boost/filesystem.hpp>

namespace ac {
  class TempDir {
  public:
    TempDir();

    TempDir(const TempDir&) = delete;
    TempDir operator=(const TempDir&) = delete;

    TempDir(TempDir&&);
    TempDir operator=(TempDir&&);

    ~TempDir();

    std::string path() const;

  private:
    boost::filesystem::path path_;
  };
}

#endif
