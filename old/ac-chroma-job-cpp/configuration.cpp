#include <fstream>
#include <iostream>
#include <stdexcept>
#include <mutex>

#include <boost/regex.hpp>
#include <unistd.h>

#include "util.hpp"
#include "configuration.hpp"

const std::string filelist_fname = "filelist";
const std::string done_fname = "donelist";
const std::string prob_fname = "problist";
const std::string inprog_fname = "inproglist";

#ifdef HAVE_FILE_LOCKS
mutex_type Configuration::mutex_ {"filelist"};
#else
mutex_type Configuration::mutex_ {boost::interprocess::open_or_create, "filelist"};
#endif



Configuration::Configuration()
  : path_{""},
    source_{0},
    dims_{0, 0, 0, 0}
    {}

Configuration::Configuration(const std::size_t& n_sources)
  : Configuration()
{

  std::clog << "Waiting for mutex..." << std::endl;
  std::unique_lock<mutex_type> lock(mutex_);

  const auto done_confs = ac::getlines(std::ifstream{done_fname});
  const auto prob_confs = ac::getlines(std::ifstream{prob_fname});
  const auto inprog_confs = ac::getlines(std::ifstream{inprog_fname});
  const auto all_confs = ac::getlines(std::ifstream{filelist_fname});

  for (const auto& filename : all_confs) {
    const auto bname = ac::basename(filename);
    if (!ac::contains(prob_confs, bname)) {
      auto n_done_or_doing = (ac::count(inprog_confs, bname)
                              + ac::count(done_confs, bname));
      if (n_done_or_doing < n_sources) {
        ac::append(inprog_fname, bname);
        path_ = ac::canonicalpath(filename);
        source_ = n_sources - n_done_or_doing;
        break;
      }
    }
  }

  // Throw error if none found
  if (path_ == "")
    throw std::runtime_error{"No more configurations remaining"};
}

Configuration::~Configuration() {

  std::clog << "Waiting for mutex..." << std::endl;
  std::unique_lock<mutex_type> lock(mutex_);

  // Create new file with offending line removed
  {
    std::ifstream oldfile(inprog_fname);
    std::ofstream newfile(inprog_fname + ".tmp");
    auto removed = false;
    const auto bname = ac::basename(path_);
    for (std::string line; std::getline(oldfile, line); ) {
      if (!removed && line == bname)
        removed = true;
      else
        newfile << line << std::endl;
    }
  }

  // Replace original file with new file
  ac::rename(inprog_fname + ".tmp", inprog_fname);
}

Configuration::Configuration(Configuration&& other) : Configuration() {
  using std::swap;
  swap(path_, other.path_);
  swap(source_, other.source_);
  swap(dims_, other.dims_);
}

std::vector<int> Configuration::dims() const {
  if (dims_[0] != 0)
    return dims_;

  static const std::string regex {"\\s*<lx>(\\d*)</lx>\\s*<ly>(\\d*)</ly>\\s*<lz>(\\d*)</lz>\\s*<lt>(\\d*)</lt>"};
  std::vector<std::string> matches;
  std::ifstream infile{path_};
  for (std::string line; std::getline(infile, line); ) {
    if (ac::match(line, regex, matches)) {
      dims_ = {std::stoi(matches[1]),
               std::stoi(matches[2]),
               std::stoi(matches[3]),
               std::stoi(matches[4])};
      return dims_;
    }
  }

  throw std::runtime_error{"Could not parse lattice dimensions"};

}

int Configuration::source() const {
  return source_;
}

std::string Configuration::path() const {return path_;}

std::string Configuration::name() const {
  std::vector<std::string> match;
  ac::match(path_, ".*?(qcdsf\\.\\d*\\.\\d*)\\.lime", match);
  return match[1];
}

void Configuration::mark_done() const {
  ac::append(done_fname, ac::basename(path_));
}

void Configuration::mark_problem() const {
  ac::append(prob_fname, ac::basename(path_));
}
