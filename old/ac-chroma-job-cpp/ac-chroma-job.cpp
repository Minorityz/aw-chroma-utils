#include <iostream>
#include <fstream>
#include <string>
#include <stdexcept>
#include <cstdlib>
#include <chrono>

#ifdef HAVE_BOOST_PO
#include <boost/program_options.hpp>
#endif

#include "util.hpp"
#include "configuration.hpp"
#include "input_template.hpp"

int main(int argc, char** argv)
{
  using Clock = std::chrono::system_clock;

  const auto start_time = Clock::now();

  std::clog << std::string(80, '=') << std::endl;
  std::clog << "ac-chroma-job - Job director" << std::endl;
  std::clog << std::string(80, '=') << std::endl;

  #ifdef HAVE_BOOST_PO

  namespace bspo = boost::program_options;
  bspo::options_description op_desc{"Allowed options"};
  op_desc.add_options()
    ("help,h", "Show this message")
    ("templates", bspo::value<std::vector<std::string>>(),
     "Chroma input templates")
    ("runs", bspo::value<int>()->default_value(1),
     "Number of runs to perform")
    ("sources", bspo::value<std::size_t>()->default_value(1),
     "Number of sources per configuration")
    ;
  bspo::positional_options_description pop_desc;
  pop_desc.add("templates", -1);
  bspo::variables_map op_map;
  bspo::store(bspo::command_line_parser(argc, argv)
  	      .options(op_desc)
  	      .positional(pop_desc)
  	      .run(),
  	      op_map);
  bspo::notify(op_map);

  if(op_map.count("help")) {
    std::cout << op_desc << std::endl;
    return 0;
  }

  const auto input_template_filenames = op_map["templates"].as<std::vector<std::string>>();
  const auto n_runs = op_map["runs"].as<int>();
  const auto n_sources = op_map["sources"].as<std::size_t>();

  #else

  const auto arg_vector = ac::arguments(argc, argv);
  const auto n_runs = std::stoi(arg_vector[0]);
  const std::size_t n_sources {static_cast<std::size_t>(std::stoi(arg_vector[1]))};
  const auto input_template_filenames = ac::slice(arg_vector, 2, arg_vector.size());

  #endif

  std::clog << "Input templates: " << input_template_filenames << std::endl;
  std::clog << "Number of runs: " << n_runs << std::endl;
  std::clog << "Number of sources: " << n_sources << std::endl;

  const std::string input_dir = ac::absolutepath("input");
  ac::mkdir(input_dir);
  std::clog << "Input directory: " << input_dir << std::endl;

  const std::string output_dir = ac::absolutepath("output");
  ac::mkdir(output_dir);
  std::clog << "Output directory: " << output_dir << std::endl;

  const std::string seed (ac::firstline(std::ifstream("seed")));
  if (seed == "")
    throw std::runtime_error{"No seed found"};
  std::clog << "Seed: " << seed << std::endl;

  std::vector<InputTemplate> input_templates;
  for (const auto& input_template_filename : input_template_filenames)
    input_templates.push_back(InputTemplate{input_template_filename});

  for (int i = 0; i != n_runs; i++) {

    const auto run_start_time = Clock::now();

    if (ac::exists("STOP"))
      return 1;
    if (ac::exists("RESTART"))
      return 0;

    const ac::TempDir temp_dir{};
    std::clog << "Temporary directory: " << temp_dir.path() << std::endl;

    // Check for STOP_N file
    // Check for RESTART_N file

    const Configuration conf {n_sources};
    std::clog << "Next configuration: " << conf.path() << std::endl;

    std::string chroma_invocation {"ac-chroma"};
    for (const auto& input_template : input_templates) {
      auto input_filename = input_template.write(input_dir,
						       output_dir,
						       temp_dir.path(),
						       conf,
						       seed);
      std::clog << "Created input file: " << input_filename << std::endl;

      auto output_filename = ac::basename(input_filename);
      if (ac::contains(input_filename, "input"))
	ac::replace(output_filename, "input", "output");
      else
	output_filename = "output" + input_filename;
      output_filename = output_dir + "/" + output_filename;

      chroma_invocation += " \\\n" + input_filename;
      chroma_invocation += " \\\n" + output_filename;
    }
    chroma_invocation += " \\\n 1>&2";
    std::clog << "Chroma invocation: \"" << chroma_invocation << "\"" << std::endl;

    std::clog << "Passing execution to ac-chroma..." << std::endl;
    const auto chroma_return = system(chroma_invocation.c_str());
    std::clog << "ac-chroma return: " << chroma_return << std::endl;

    const auto run_end_time = Clock::now();
    std::clog << "Run duration: "
	      << std::chrono::duration_cast<std::chrono::minutes>
      (run_end_time - run_start_time).count()
	      << " min"
	      << std::endl;

    if(chroma_return == 0) {
      conf.mark_done();
      std::clog << "Run successful!" << std::endl;
    } else {
      conf.mark_problem();
      std::clog << "Run failed!" << std::endl;
      return chroma_return;
    }


  }
  std::clog << "Job director finished" << std::endl;

  const auto end_time = Clock::now();
  std::clog << "Job duration: "
	    << std::chrono::duration_cast<std::chrono::minutes>
    (end_time - start_time).count()
	    << " min"
	    << std::endl;

  return 0;
}
