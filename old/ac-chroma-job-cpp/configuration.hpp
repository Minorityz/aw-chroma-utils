#ifndef CONFIGURATION_HPP
#define CONFIGURATION_HPP

#include <string>
#include <vector>

#ifdef HAVE_FILE_LOCKS

#include <boost/interprocess/sync/file_lock.hpp>
using mutex_type = boost::interprocess::file_lock;

#else

#include <boost/interprocess/sync/named_mutex.hpp>
using mutex_type = boost::interprocess::named_mutex;

#endif


class Configuration
{
public:
  Configuration();
  explicit Configuration(const std::size_t& n_sources);

  Configuration(const Configuration&) = delete;
  Configuration operator=(const Configuration&) = delete;

  Configuration(Configuration&&);
  Configuration operator=(Configuration&&);

  ~Configuration();

  std::string path() const;
  std::string name() const;
  std::vector<int> dims() const;
  int source() const;

  void mark_done() const;
  void mark_problem() const;


private:
  std::string path_;
  int source_;
  mutable std::vector<int> dims_;

  static mutex_type mutex_;
};

#endif
