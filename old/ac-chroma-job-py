#!/usr/bin/env python3

import glob
import random
import argparse
import os
import tempfile
import re
import time
import cmath
import fcntl
import sys
import collections
import logging
import signal


# Setup logger and logger output format
logger = logging.getLogger()
logger.setLevel(logging.DEBUG)
handler = logging.StreamHandler()
handler.setLevel(logging.INFO)
formatter = logging.Formatter(':'.join([
    '%(levelname)s',
    '%(name)s',
    '%(lineno)d',
    '%(funcName)s',
    '%(message)s',
    ]))
handler.setFormatter(formatter)
logger.addHandler(handler)


# Setup signal handler to interrupt SIGTERM (if job exceeds walltime)
def signal_handler(signum, frame):
    if signum == signal.SIGTERM:
        logger.info('Received SIGTERM signal, exiting...')
        sys.exit(1)
    else:
        logger.error('Signal %r received, but don\'t know how to handle it!'
                     % signum)
signal.signal(signal.SIGTERM, signal_handler)


class LockFile:
    def __init__(self, path):
        self.path = path
        self.fp = None
        self._locked = False

    def lock(self):
        logger.info('Attempting to lock LockFile...')
        if self._locked:
            raise RuntimeError('Cannot lock an already locked LockFile')
        else:
            self.fp = open(self.path, 'a+')
            fcntl.lockf(self.fp, fcntl.LOCK_EX)
            self._locked = True

    def unlock(self):
        logger.info('Attempting to unlock LockFile...')
        if self._locked:
            fcntl.lockf(self.fp, fcntl.LOCK_UN)
            self.fp.close()
            self._locked = False
        else:
            raise RuntimeError('Cannot unlock an already unlocked LockFile')

    def __enter__(self):
        if not self._locked:
            self.lock()

    def __exit__(self, exc_type, exc_val, traceback):
        if self._locked:
            self.unlock()


LOCK_FILE = LockFile('.lock')


class ActiveConfiguration:
    def __init__(self, filename, nth_source, inprog_filename, prob_filename):
        logger.info('Creating Active Configuration...')

        self.filename = filename
        logger.info('Filename: %r', self.filename)
        self.name = confname(filename)
        logger.info('Name: %r', self.name)
        logger.info('Parsing lattice dimensions...')
        self.dims = parse_dims(filename)
        logger.info('Dimensions: %r', self.dims)

        self.nth_source = nth_source
        logger.info('Source number: %r', self.nth_source)

        self.inprog_filename = inprog_filename
        self.prob_filename = prob_filename

        self._active = False
        self.activate()

    def activate(self):
        if self._active:
            raise RuntimeError('Can\'t activate a configuration that is already active!')
        else:
            logger.info('Appending configuration to in-progress list...')
            append_to_file(self.inprog_filename, self.name)
            self._active = True

    def deactivate(self):
        if self._active:
            logger.info('Removing configuration from in-progress list...')
            remove_from_file(self.inprog_filename, self.name)
            self._active = False
        else:
            raise RuntimeError('Can\'t deactivate a configuration if it isn\'t active yet!')

    def __enter__(self):
        if not self._active:
            self.activate()
        return self

    def __exit__(self, exc_type, exc_val, traceback):
        logger.info('Exiting ActiveConfiguration context')
        logger.info('Exception type: %r', exc_type)
        if self._active:
            self.deactivate()
        if exc_type is not None and exc_type not in (SystemExit,):
            logger.info('Appending configuration to problem list...')
            append_to_file(self.prob_filename, self.name)


def parse_cl_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--n-sources', action='store', type=int, default=1)
    parser.add_argument('--n-runs', action='store', type=int, default=1)
    args = parser.parse_args()
    if args.n_sources < 1:
        raise ValueError('Number of sources must be greater than zero')
    if args.n_runs < 1:
        raise ValueError('Number of runs must be greater than zero')
    return args


def remove_from_file(filename, string):
    with open(filename, 'r') as f:
        lines = f.readlines()
    with open(filename, 'w') as f:
        for line in lines:
            if string not in line:
                f.write(line)


def random_source(dims):
    return tuple(random.randint(0, n) for n in dims)


def random_z_noise(n):
    nth_roots = [cmath.exp(2*cmath.pi*1j*k/n) for k in range(0, n)]
    return nth_roots[random.randint(0, n-1)]


def first_line(filename):
    with open(filename, 'r') as infile:
        return infile.readline().rstrip('\n')


def confname(filename):
    match = re.match('.*(qcdsf\.\d*\.\d*).*', filename)
    if match:
        return match.group(1)
    raise ValueError('Filename %r does not match expected ',
                     'format for a configuration filename'
                     % filename)


def all_lines(filename):
    with open(filename) as f:
        return [l.rstrip('\n') for l in f]


def all_lines_count(filename):
    lines = {}
    with open(filename) as infile:
        for line in infile:
            line = line.rstrip('\n')
            if line in lines:
                lines[line] += 1
            else:
                lines[line] = 1
    return lines


def add_dicts(dict1, dict2):
    res = dict1.copy()
    for key in dict2:
        if key in res:
            res[key] += dict2[key]
        else:
            res[key] = dict2[key]
    return res


def all_text(filename):
    with open(filename) as f:
        return f.read()


def append_to_file(filename, line):
    with open(filename, 'a') as f:
        f.write(line.rstrip('\n') + '\n')


def parse_dims(filename):
    with open(filename, 'rb') as f:
        for line in f:
            try:
                line = line.decode('utf8')
            except UnicodeDecodeError:
                continue
            match = re.search(parse_dims.dims_re, line)
            if match:
                return tuple(
                    int(d) for d in
                    [match.group(i) for i in range(1, 5)])
    raise ValueError('Could not find lattice dimensions in file')
parse_dims.dims_re = re.compile(
    '\s*'.join('<lD>\s*(\d*)\s*</lD>'.replace('D', d) for d in 'xyzt'))


def remove_ext(filename):
    root, ext = os.path.splitext(filename)
    return root if not ext else remove_ext(root)


def create_if_needed(filename):
    with open(filename, 'a+'):
        pass


def next_configuration(filelist_filename,
                       inproglist_filename,
                       donelist_filename,
                       problist_filename,
                       deadlist_filename,
                       n_sources):
    logger.info('Looking for next configuration')
    logger.info('Sources per configuration: %r', n_sources)
    """
    Find next configuration to run given the input list filenames and the number of sources to run per configuration
    """

    logger.info('Waiting for lock to be free...')
    with LOCK_FILE:
        logger.info('Lock obtained')

        logger.info('Reading file list...')
        master_conf_filename_list = all_lines(filelist_filename)
        logger.info('%r configurations found' % len(master_conf_filename_list))

        logger.info('Reading problem list and dead list...')
        bad_conf_name_list = all_lines(problist_filename) + all_lines(deadlist_filename)
        logger.info('%r bad configurations found' % len(bad_conf_name_list))

        logger.info('Creating list of good configurations...')
        good_conf_filename_list = [cf
                                   for cf in master_conf_filename_list
                                   if confname(cf) not in bad_conf_name_list]
        logger.info('%r good configurations found' % len(good_conf_filename_list))

        logger.info('Reading and counting in-progress list and done list...')
        conf_names_and_counts = add_dicts(all_lines_count(inproglist_filename),
                                          all_lines_count(donelist_filename))

        logger.info('Trying to find next configuration that needs work...')
        for conf_filename in good_conf_filename_list:
            conf_name = confname(conf_filename)
            n_sources_done = conf_names_and_counts.get(conf_name, 0)
            n_sources_to_go = n_sources - n_sources_done
            if n_sources_to_go > 0:
                logger.info('Next configuration: %r' % conf_filename)
                return ActiveConfiguration(
                    conf_filename,
                    n_sources_done,
                    inproglist_filename,
                    problist_filename,
                    )
    raise StopIteration


start_time = time.time()

logger.info('Checking environment...')
for key in os.environ:
    logger.info('%s : %s' % (key, os.environ[key]))

pwd = os.getcwd()
logger.info('Starting in %r', pwd)

cl_input = parse_cl_args()
n_runs = cl_input.n_runs
logger.info('Number of runs: %d', n_runs)
n_sources = cl_input.n_sources
logger.info('Number of sources: %d', n_sources)

filelist_filename = 'filelist'
logger.info('File list filename: %r', filelist_filename)
inproglist_filename = 'inproglist'
logger.info('In-progress list filename: %r', inproglist_filename)
deadlist_filename = 'deadlist'
logger.info('Dead list filename: %r', deadlist_filename)
donelist_filename = 'donelist'
logger.info('Done list filename: %r', donelist_filename)
problist_filename = 'problist'
logger.info('Problem list filename: %r', problist_filename)
for fname in [filelist_filename,
              inproglist_filename,
              deadlist_filename,
              donelist_filename,
              problist_filename]:
    create_if_needed(fname)
seed_filename = 'seed'
logger.info('Seed filename: %r', seed_filename)

input_template_filenames = sorted([os.path.realpath(it)
                                   for it in glob.glob('*.xml.tmpl')])
logging.info('Glob results: %r', input_template_filenames)
if not input_template_filenames:
    raise ValueError('Must be some input templates!')
input_templates = collections.OrderedDict(
    [(remove_ext(os.path.basename(f)), all_text(f))
     for f in input_template_filenames])
for it, n in zip(input_template_filenames, input_templates):
    logger.info('Input template: %r', it)
    logger.info('Name: %r', n)

seed = first_line(seed_filename).strip()
logger.info('Seed: %r', seed)

output_dir = os.path.realpath('output')
logger.info('Output directory: %r', output_dir)
try:
    os.mkdir(output_dir)
except FileExistsError:
    pass

for irun in range(n_runs):
    logger.info('Checking for stop file...')
    if os.path.isfile('STOP'):
        logger.info('Stop file detected')
        sys.exit(1)
    else:
        logger.info('No stop file detected')

    logger.info('Checking for restart n file...')
    if os.path.isfile('RESTART_N'):
        logger.info('Restart n file detected')
        with LOCK_FILE:
            with open('RESTART_N') as infile:
                n_to_restart = int(infile.read())
            logger.info('%r jobs to be restarted', n_to_restart)
            if n_to_restart > 0:
                n_to_restart -= 1
                with open('RESTART_N', 'w') as outfile:
                    outfile.write(str(n_to_restart) + '\n')
                logger.info('Job restart requested')
                sys.exit(0)

    logger.info('Checking for stop n file...')
    if os.path.isfile('STOP_N'):
        logger.info('Stop n file detected')
        with LOCK_FILE:
            with open('STOP_N') as infile:
                n_to_stop = int(infile.read())
            logger.info('%r jobs to be stopped', n_to_stop)
            if n_to_stop > 0:
                n_to_stop -= 1
                with open('STOP_N', 'w') as outfile:
                    outfile.write(str(n_to_stop) + '\n')
                logger.info('Job stop requested')
                sys.exit(1)

    logger.info('Starting run %r', irun)

    with next_configuration(filelist_filename,
                            inproglist_filename,
                            donelist_filename,
                            problist_filename,
                            deadlist_filename,
                            n_sources) as conf:

        combined_seed = seed + conf.name + str(conf.nth_source)
        logger.info('Combined seed: %r' % combined_seed)
        random.seed(seed + conf.name + str(conf.nth_source))

        source = random_source(conf.dims)
        logger.info('Source: %r', source)
        source_tag = '_'.join(str(d) for d in source)
        logger.info('Source tag: %r', source_tag)

        with tempfile.TemporaryDirectory(dir=pwd) as scratch_dir:
            logger.info('Scratch directory: %r', scratch_dir)
            input_filenames = []
            output_filenames = []
            for input_name in input_templates:
                logger.info('Filling template %r...', input_name)
                input_basename = '_'.join(
                    [input_name, conf.name, source_tag, '.xml'])
                output_basename = input_basename.replace('input', 'output')

                input_filename = os.path.join(output_dir, input_basename)
                output_filename = os.path.join(output_dir, output_basename)

                text = input_templates[input_name]

                text = text.replace('XXTRAJXX', '_'.join((conf.name, source_tag)))
                text = text.replace('XXCONFXX', conf.filename)
                text = text.replace('XXRESXX', output_dir)
                text = text.replace('XXTMPXX', scratch_dir)
                text = text.replace('XXNXXX', str(conf.dims[0]))
                text = text.replace('XXNYXX', str(conf.dims[1]))
                text = text.replace('XXNZXX', str(conf.dims[2]))
                text = text.replace('XXNTXX', str(conf.dims[3]))
                text = text.replace('XXSXXX', str(source[0]))
                text = text.replace('XXSYXX', str(source[1]))
                text = text.replace('XXSZXX', str(source[2]))
                text = text.replace('XXSTXX', str(source[3]))

                while True:
                    match = re.search('XXZ(\d*)NOISE(.*?)REALXX', text)
                    if match:
                        logger.info('Found noise template: %r', match.group(0))
                        n = int(match.group(1))
                        logger.info('Noise degree: %r', n)
                        noise_id = match.group(2)
                        logger.info('Id: %r', noise_id)
                        z_noise = random_z_noise(n)
                        logger.info('Filling with: %r', z_noise)
                        real_repl = str(z_noise.real)
                        imag_repl = str(z_noise.imag)
                        real_str = ('XXZ' + str(n) + 'NOISE' + noise_id
                                    + 'REALXX')
                        imag_str = ('XXZ' + str(n) + 'NOISE' + noise_id
                                    + 'IMAGXX')
                        text = text.replace(real_str, real_repl)
                        text = text.replace(imag_str, imag_repl)
                    else:
                        break

                with open(input_filename, 'w') as outfile:
                    outfile.write(text)

                input_filenames.append(input_filename)
                output_filenames.append(output_filename)

            chroma_invocation = ' '.join(['ac-chroma']
                                            + input_filenames
                                            + output_filenames)
            logger.info('Chroma invocation: %r', chroma_invocation)

            logger.info('Running Chroma...')
            if os.system(chroma_invocation) == 0:
                logger.info('Chroma ran successfully')
                logger.info('Appending configuration to done list...')
                append_to_file(donelist_filename, conf.name)
            else:
                raise RuntimeError('Problem running chroma')




end_time = time.time()
total_time = end_time - start_time
total_time_min = total_time/60
logger.info('Job ran for %r minutes', round(total_time_min))
